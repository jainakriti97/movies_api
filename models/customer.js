const mongoose = require('mongoose');
const Joi = require('joi');

const customerSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    isPremium: {
        type: String,
        default: false
    },
    phone: {
        type: String,
        required: true
    }
})

const Customer = mongoose.model('Customer', customerSchema);

function validateCustomer(customer){
    const schema={
        name:Joi.string().required(),
        isPremium:Joi.string(),
        phone:Joi.string().required()
    };
    return Joi.validate(customer,schema);
  }

  exports.validate=validateCustomer;
  exports.Customer=Customer;