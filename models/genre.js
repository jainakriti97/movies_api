const Joi = require('joi');
const mongoose=require('mongoose');

const genreSchema=new mongoose.Schema({
    name:{
        type:String,
    required:true
    
    }
});
const Genre=mongoose.model('genre',genreSchema);

function ValidateGenre(genre){
    const schema={
        name: Joi.string().required()
    };
    return Joi.validate(genre,schema);
}

exports.validate=ValidateGenre;
exports.Genre=Genre;
exports.genreSchema=genreSchema;