const Joi = require('joi');
const mongoose = require('mongoose');
// const {genreSchema,Genre} = require('./genre');

const movieCollection = new mongoose.Schema({

    title: {
        type: String,
        required: true
    },
    genre: {

        type: mongoose.Schema.Types.ObjectId,
        ref: 'genre',
        required: true
    },

    numberInStock: {
        type: Number,
        required: true
    },

    dailyRentalRate: {

        type: Number,
        required: true
    }
});

const Movie = mongoose.model('movie', movieCollection);

function validateMovie(movie) {
    const Schema = {
        title: Joi.string().required(),
        genre: Joi.string().required(),
        numberInStock: Joi.number().required(),
        dailyRentalRate: Joi.number().required()
    };
    return Joi.validate(movie, Schema);

}

exports.Movie = Movie;
exports.validate = validateMovie;