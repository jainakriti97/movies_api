const express = require('express');
const {validate,Customer}=require('../models/customer');
const router = express.Router();


router.get('/', async (req, res) => {
    const customers = await Customer.find().sort('name');
    res.send(customers)
})

router.post('/', async (req, res) => {
    const {
        error
    } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    let customer =new Customer({
        name: req.body.name,
        isPremium:req.body.isPremium,
        phone:req.body.phone
    })
customer=await customer.save();
    res.send(customer);
})

router.put('/',async(req,res)=>{
    const {error}=validate(req.body);
    if(error) return res.status(400).send(error.detail[0].message);
    const customer=await Customer.findByIdAndUpdate(req.params.id,{name:req.body.name,
        isPremium:req.body.isPremium,
        phone:req.body.phone  
    })
    if(!customer) return res.status(404).send("customer id not found");
    res.send(customer);
});

router.delete('/',async(req,res)=>{
    const customer= await Customer.findByIdAndRemove(req.params.id);
    if(!customer) return res.status(404).send("customer id not found");
    res.send(customer);
});

router.get('/:id',async(req,res)=>{
    const customer=await Customer.findById(req.params.id);
    if(!customer) return res.status(404).send("customer id not found");
    res.send(customer);
})


module.exports=router;